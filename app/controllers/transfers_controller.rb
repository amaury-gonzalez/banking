class TransfersController < ApplicationController
  before_action :set_transfer, only: [:show, :edit, :update, :destroy]

  # GET /transfers/new
  def new
    @origin_account = Account.find(params[:account_id])
    @transfer = Transfer.new(origin_account: @origin_account)
    @destiny_accounts = Account.where.not(id: @origin_account.id)
  end

  # POST /transfers
  def create
    @transfer = Transfer.new(transfer_params)

    if @transfer.save
      redirect_to @transfer.origin_account, notice: "#{@transfer.origin_account.owner}, Your transfer has been made"
    else
      render :new
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transfer
      @transfer = Transfer.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def transfer_params
      params[:transfer].permit(:money, :origin_account_id, :destiny_account_id)
    end
end
