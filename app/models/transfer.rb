class Transfer < ActiveRecord::Base

  belongs_to :origin_account, :class_name => 'Account'
  belongs_to :destiny_account, :class_name => 'Account'

  monetize :money_cents, allow_nil: false, numericality: { :greater_than => 0 }
  monetize :commission_cents, allow_nil: false, numericality: { greater_than_or_equal_to: 0 }
  validate :origin_and_destiny_is_present
  validate :origin_and_destiny_cannot_be_the_same
  validate :interbank_cannot_be_lower_than_1000
  validates :state, presence: true, inclusion: { in: %w(succeed failed initiated) }

  scope :successful, -> { where(state: 'succeed') }
  scope :failed, -> { where(state: 'failed') }

  before_save :apply_transfer

  private

  def intra_bank
    self.origin_account.bank.name == self.destiny_account.bank.name
  end

  def has_origin_and_destiny_account
    self.origin_account.present? && self.destiny_account.present?
  end

  def origin_and_destiny_is_present
    unless has_origin_and_destiny_account
      self.errors[:base] << 'Transaction must have origin and destiny account'
      return
    end
  end

  def interbank_cannot_be_lower_than_1000
    if has_origin_and_destiny_account
      unless intra_bank
        #Money gem deals with cents €1000 = 1000 *100 = 100000
        if money >= Money.new(100000)
          errors.add(:money, 'Intra-bank transactions must be lower than 1000')
        end
      end
    end
  end

  def origin_and_destiny_cannot_be_the_same
    if self.origin_account == self.destiny_account
      errors.add(:origin_account, 'must be different to destiny account')
      errors.add(:destiny_account, 'must be different to origin_account')
    end
  end

  # Intra-bank transfers, between accounts of the same bank. They don't have
  # commissions, they don't have limits and they always succeed.
  # Inter-bank transfers, between accounts of different banks. They have 5€
  # commissions, they have a limit of 1000€ per transfer. And they have a 30%
  # chance of failure.
  def apply_transfer
    if intra_bank
      self.origin_account.subtract(self.money)
      self.destiny_account.deposit(self.money)
      self.commission = 0
      self.state = 'succeed'
    else
      # 30% chance of failure.
      if rand(100) > 30
        self.origin_account.subtract(self.money + Money.new(500))
        self.destiny_account.deposit(self.money)
        self.commission = Money.new(500)
        self.state = 'succeed'
      else
        self.state = 'failed'
      end
    end
  end


end
