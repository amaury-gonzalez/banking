class Bank < ActiveRecord::Base

  has_many :accounts

  validates :name, presence: true, uniqueness: true

  def transfer_history
    transaction = []
    accounts.each do |a|
      transaction.push(Transfer.where("origin_account_id = #{a.id} or destiny_account_id = #{a.id}"))
    end
    transaction.first.to_a.uniq .sort_by &:created_at
  end

  def transfer_history_of(account)
    if self.accounts.include?(account)
      transaction = []
      transaction.push(Transfer.where("origin_account_id = #{account.id} or destiny_account_id = #{account.id}"))
      transaction.first.to_a.uniq .sort_by &:created_at
    else
      raise "This account doesn't exist in this bank"
    end

  end

end
