class Account < ActiveRecord::Base

  belongs_to :bank

  attr_accessor :money # For testing purposes

  validates_presence_of :bank
  validates :owner, presence: true
  validates :number, presence: true, uniqueness: true
  monetize :money_cents, :allow_nil => false, :numericality => { :greater_than_or_equal_to => 0 }

  def print
    "#{self.owner}(#{self.number})"
  end
  def deposit(ammount)
    self.money = self.money + ammount
  end

  def subtract(ammount)
    if self.money > ammount
      self.money = self.money - ammount
    else
      raise('Enough charge in the account to do this operation')
    end
  end

  def transfer(destiny_account, money)
    transfer = Transfer.new(origin_account: self, destiny_account: destiny_account, money: money)
    transfer.save
  end
end
