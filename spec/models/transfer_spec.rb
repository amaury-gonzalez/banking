require 'rails_helper'

RSpec.describe Transfer, type: :model do
  describe do
    before(:each) do
      @transfer = FactoryGirl.build(:transfer,
                                    origin_account: FactoryGirl.create(:account1_bank1),
                                    destiny_account: FactoryGirl.create(:account2_bank1),
                                    money: Money.new(100),
                                    state: 'succeed')
    end

    it 'is valid with valid attributes' do
      expect(@transfer).to be_valid
    end

    it 'is not valid without a origin account' do
      @transfer.origin_account = nil
      expect(@transfer).not_to be_valid
    end

    it 'is not valid without a destiny account' do
      @transfer.destiny_account = nil
      expect(@transfer).not_to be_valid
    end

    it 'is not valid with origin equal to destiny account' do
      @transfer.origin_account = @transfer.destiny_account
      expect(@transfer).not_to be_valid
    end

    it 'is not valid without valid money' do
      @transfer.money = nil
      expect(@transfer).not_to be_valid
      @transfer.money = Money.new(-1)
      expect(@transfer).not_to be_valid
    end

    it 'is not valid without a state' do
      @transfer.state = nil
      expect(@transfer).not_to be_valid
    end
  end

  describe 'intra-bank' do
    it 'is saved successfully changing correctly the origin and destiny account' do
      intrabank_transfer = FactoryGirl.build(:transfer,
                                             origin_account: FactoryGirl.create(:account1_bank1),
                                             destiny_account: FactoryGirl.create(:account2_bank1)
                                            )
      origin_account_money = intrabank_transfer.origin_account.money
      destiny_account_money = intrabank_transfer.destiny_account.money
      intrabank_transfer.save!
      expect(intrabank_transfer.origin_account.money).to eq(origin_account_money - intrabank_transfer.money)
      expect(intrabank_transfer.destiny_account.money).to eq(destiny_account_money + intrabank_transfer.money)
      expect(intrabank_transfer.state).to eq('succeed')
    end
  end

  describe 'inter-bank' do
    it 'is not valid if the amount is higher than 1000' do
      interbank_transfer = FactoryGirl.create(:transfer,
                                              origin_account: FactoryGirl.create(:account1_bank1),
                                              destiny_account: FactoryGirl.create(:account2_bank2))
      interbank_transfer.money = Money.new(1001 * 100)
      expect(interbank_transfer).not_to be_valid
    end

    it 'is saved successfully changing accounts depending of their success' do
      interbank_transfer = FactoryGirl.build(:transfer,
                                             origin_account: FactoryGirl.create(:account1_bank1),
                                             destiny_account: FactoryGirl.create(:account2_bank2),
                                             money: Money.new(500))

      origin_account_money = interbank_transfer.origin_account.money
      destiny_account_money = interbank_transfer.destiny_account.money
      interbank_transfer.save!

      if interbank_transfer.state == 'succeed'
        commission = Money.new(500)
        # The Commission is charged to the origin account
        expect(interbank_transfer.origin_account.money).to eq(origin_account_money - (interbank_transfer.money + commission))
        expect(interbank_transfer.destiny_account.money).to eq(destiny_account_money + interbank_transfer.money)
        expect(interbank_transfer.commission).to eq(commission)
      else
        # If the transaction failed the money is not charged
        expect(interbank_transfer.origin_account.money).to eq(origin_account_money)
        expect(interbank_transfer.destiny_account.money).to eq(destiny_account_money)
        expect(interbank_transfer.commission).to eq(0)
      end

    end
  end

end
