require 'rails_helper'

RSpec.describe Account, type: :model do
  before(:each) do
    @account = FactoryGirl.create(:account1_bank1)
  end


  it 'is not valid without valid money' do
    @account.money_cents = nil
    expect(@account).not_to be_valid
    @account.money_cents = -1
    expect(@account).not_to be_valid
  end

  it 'cannot do transfers without enough balance' do
    @account.money = Money.new(500)
    expect{@account.subtract(Money.new(501))}.to raise_error(RuntimeError)
  end

  it '#transfer' do
    destiny_account = FactoryGirl.create(:account2_bank1)
    @account.transfer(destiny_account, Money.new(500))
    transfer = Transfer.last
    expect(transfer.origin_account).to eq(@account)
    expect(transfer.destiny_account).to eq(destiny_account)
    expect(transfer.money.cents).to eq(500)
  end
end
