require 'rails_helper'

RSpec.describe Bank, type: :model do
  before(:each) do
    @bank1 = FactoryGirl.build(:bank1)
  end

  it 'adds a account' do
    @bank1.accounts = []
    account1 = FactoryGirl.create(:account1_bank1)
    @bank1.accounts << account1
    expect(@bank1.accounts).to include(account1)
  end

  it 'gives the transfer history of all their accounts' do
    account1 = FactoryGirl.create(:account1_bank1)
    account2 = FactoryGirl.create(:account2_bank1)
    @bank1.accounts = [account1, account2]
    #Generating 10 back and forth transaction
    money = Money.new(100)
    history = []
    1.upto(5).each do |i|
      history << FactoryGirl.create(:transfer, origin_account: account1, destiny_account: account2, money: money)
      history << FactoryGirl.create(:transfer, origin_account: account2, destiny_account: account1, money: money)
    end
    expect(@bank1.transfer_history).to match_array(history)
  end

  it 'gives the transfer history of a account' do
    account1 = FactoryGirl.create(:account1_bank1)
    account2 = FactoryGirl.create(:account2_bank2)
    expect{@bank1.transfer_history_of(account2)}.to raise_error("This account doesn't exist in this bank")

    @bank1.accounts = [account1]
    #Generating 10 back and forth transaction
    money = Money.new(100)
    history = []
    1.upto(5).each do |i|
      history << FactoryGirl.create(:transfer, origin_account: account1, destiny_account: account2, money: money)
      history << FactoryGirl.create(:transfer, origin_account: account2, destiny_account: account1, money: money)
    end
    expect(@bank1.transfer_history_of(account1)).to match_array(history)
  end

end
