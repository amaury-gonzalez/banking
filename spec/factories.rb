# This will guess the User class
FactoryGirl.define do

  factory :bank do
    factory :bank1 do
      name Faker::Bank.unique.name
    end
    factory :bank2 do
      name Faker::Bank.unique.name
    end
  end

  factory :account do
    money 1000

    trait :account_jhon do
      owner Faker::Name.unique.name
      number 'ES76DZJM33188515981980'
    end

    trait :account_jane do
      owner Faker::Name.unique.name
      number 'ES76DZJM33188515981979'
    end

    trait :account_bank1 do
      bank { FactoryGirl.build(:bank1) }
    end

    trait :account_bank2 do
      bank { FactoryGirl.build(:bank2) }
    end

    factory :account1_bank1, traits: [:account_jhon, :account_bank1]
    factory :account2_bank1, traits: [:account_jane, :account_bank1]
    factory :account1_bank2, traits: [:account_jhon, :account_bank2]
    factory :account2_bank2, traits: [:account_jane, :account_bank2]
  end

  factory :transfer do
    money 500
    state 'initiated'
  end

end