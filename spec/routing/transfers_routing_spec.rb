require "rails_helper"

RSpec.describe TransfersController, type: :routing do
  describe "routing" do

    it "routes to #new" do
      expect(:get => "/accounts/1/transfers/new").to route_to("transfers#new", :account_id => "1")
    end

    it "routes to #create" do
      expect(:post => "/accounts/1/transfers").to route_to("transfers#create", :account_id => "1")
    end

  end
end
