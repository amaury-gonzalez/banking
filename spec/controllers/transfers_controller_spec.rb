require 'rails_helper'

RSpec.describe TransfersController, type: :controller do

  let(:valid_attributes) {
   {
     origin_account_id: FactoryGirl.create(:account1_bank1).id,
     destiny_account_id: FactoryGirl.create(:account2_bank2).id,
     money: 500,
     state: 'initiated'
   }
  }

  let(:invalid_attributes) {
    {
      origin_account_id: nil,
      destiny_account_id: nil,
      money: 500,
      state: 'initiated'
    }
  }

  describe "GET #new" do
    it "assigns a new transfer as @transfer" do
      account = FactoryGirl.create(:account1_bank1)
      get :new, account_id: account.id
      expect(assigns(:transfer)).to be_a_new(Transfer)
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Transfer" do
        expect {
          post :create, account_id: valid_attributes[:origin_account_id], transfer: valid_attributes
        }.to change(Transfer, :count).by(1)
      end

      it "assigns a newly created transfer as @transfer" do
        post :create, account_id: valid_attributes[:origin_account_id], transfer: valid_attributes
        expect(assigns(:transfer)).to be_a(Transfer)
        expect(assigns(:transfer)).to be_persisted
      end

      it "redirects to account" do
        post :create, account_id: valid_attributes[:origin_account_id], transfer: valid_attributes
        expect(response).to redirect_to(Account.find(valid_attributes[:origin_account_id]))
      end
    end

    context "with invalid params" do
      it "assigns a newly created but unsaved transfer as @transfer" do
        post :create, account_id: valid_attributes[:origin_account_id], transfer: invalid_attributes
        expect(assigns(:transfer)).to be_a_new(Transfer)
      end

      it "re-renders the 'new' template" do
        post :create, account_id: valid_attributes[:origin_account_id], transfer: invalid_attributes
        expect(response).to render_template("new")
      end
    end
  end

end
