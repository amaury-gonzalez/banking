require 'rails_helper'

RSpec.describe BanksController, type: :controller do

  let(:valid_attributes) {
    {name: Faker::Bank.name}
  }

  let(:invalid_attributes) {
    {name: ''}
  }

  describe "GET #index" do
    it "assigns all banks as @banks" do
      bank = Bank.create! valid_attributes
      get :index, {}
      expect(assigns(:banks)).to eq([bank])
    end
  end

  describe "GET #show" do
    it "assigns the requested bank as @bank" do
      bank = Bank.create! valid_attributes
      get :show, {id: bank.to_param}
      expect(assigns(:bank)).to eq(bank)
    end
  end

  describe "GET #new" do
    it "assigns a new bank as @bank" do
      get :new, {}
      expect(assigns(:bank)).to be_a_new(Bank)
    end
  end

  describe "GET #edit" do
    it "assigns the requested bank as @bank" do
      bank = Bank.create! valid_attributes
      get :edit, {id: bank.to_param}
      expect(assigns(:bank)).to eq(bank)
    end
  end

  it "re-renders the 'edit' template" do
    bank = Bank.create! valid_attributes
    put :update, {id: bank.to_param, bank: invalid_attributes}
    expect(response).to render_template("edit")
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Bank" do
        expect {
          post :create, {bank: valid_attributes}
        }.to change(Bank, :count).by(1)
      end

      it "assigns a newly created bank as @bank" do
        post :create, {bank: valid_attributes}
        expect(assigns(:bank)).to be_a(Bank)
        expect(assigns(:bank)).to be_persisted
      end

      it "redirects to the created bank" do
        post :create, {bank: valid_attributes}
        expect(response).to redirect_to(Bank.last)
      end
    end

    context "with invalid params" do
      it "assigns a newly created but unsaved bank as @bank" do
        post :create, {bank: invalid_attributes}
        expect(assigns(:bank)).to be_a_new(Bank)
      end

      it "re-renders the 'new' template" do
        post :create, {bank: invalid_attributes}
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        {name: Faker::Bank.name}
      }

      it "updates the requested bank" do
        bank = Bank.create! valid_attributes
        put :update, {id: bank.to_param, bank: new_attributes}
        bank.reload
        expect(bank.name).to eq(new_attributes[:name])
      end

      it "assigns the requested bank as @bank" do
        bank = Bank.create! valid_attributes
        put :update, {id: bank.to_param, bank: valid_attributes}
        expect(assigns(:bank)).to eq(bank)
      end

      it "redirects to the bank" do
        bank = Bank.create! valid_attributes
        put :update, {id: bank.to_param, bank: valid_attributes}
        expect(response).to redirect_to(bank)
      end
    end

    context "with invalid params" do
      it "assigns the bank as @bank" do
        bank = Bank.create! valid_attributes
        put :update, {id: bank.to_param, bank: invalid_attributes}
        expect(assigns(:bank)).to eq(bank)
      end

    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested bank" do
      bank = Bank.create! valid_attributes
      expect {
        delete :destroy, {id: bank.to_param}
      }.to change(Bank, :count).by(-1)
    end

    it "redirects to the banks list" do
      bank = Bank.create! valid_attributes
      delete :destroy, {id: bank.to_param}
      expect(response).to redirect_to(banks_url)
    end
  end

end
