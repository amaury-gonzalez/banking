class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.belongs_to :bank, index: true
      t.string :owner
      t.string :number
      t.monetize :money

      t.timestamps null: false
    end
  end
end
