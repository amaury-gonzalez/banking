class CreateTransfers < ActiveRecord::Migration
  def change
    create_table :transfers do |t|
      t.references :origin_account
      t.references :destiny_account
      t.monetize :money
      t.monetize :commission, default: 0
      t.string :state, default: 'initiated'

      t.timestamps null: false
    end
  end
end
