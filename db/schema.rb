# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170704215313) do

  create_table "accounts", force: :cascade do |t|
    t.integer  "bank_id"
    t.string   "owner"
    t.string   "number"
    t.integer  "money_cents",    default: 0,     null: false
    t.string   "money_currency", default: "USD", null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "accounts", ["bank_id"], name: "index_accounts_on_bank_id"

  create_table "banks", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "transfers", force: :cascade do |t|
    t.integer  "origin_account_id"
    t.integer  "destiny_account_id"
    t.integer  "money_cents",         default: 0,           null: false
    t.string   "money_currency",      default: "USD",       null: false
    t.integer  "commission_cents",    default: 0,           null: false
    t.string   "commission_currency", default: "USD",       null: false
    t.string   "state",               default: "initiated"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

end
