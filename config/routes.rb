Rails.application.routes.draw do
  root 'banks#index'
  resources :accounts do
    resources :transfers, only: [:new, :create]
  end
  resources :banks
end
